<?php
/**
 * Plingconica - creating a Laconica instance full of plings data (http://plings.net)
 * Copyright (C) 2009 Ben Webb <bjwebb@freedomdreams.co.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define('INSTALLDIR', realpath(dirname(__FILE__) . '/..'));
define('LACONICA', true);

require_once "config.php";
require_once "../lib/common.php";

$las = array("doncaster", "hull", "wakefield", "blackburn", "blackpool", "bolton", "halton", "knowsley", "manchester", "salford", "stockport", "islington", "lewisham", "durham", "gateshead", "southtyneside", "leicester", "nottingham", "birmingham", "coventry", "walsall", "newcastle");

foreach ($las as $la) {
    $group = new User_group();
    
    $group->query('BEGIN');
    
    $group->nickname    = $la;
    if ($la == "southtyneside") {
        $group->fullname = "South Tyneside";
        $img = "SouthTyneside";
    }
    else {
        $group->fullname = ucwords($la);
        $img = ucwords($la);
    }
    $group->homepage    = "http://$la.placestogothingstodo.co.uk/";
    $group->description = "";
    $group->location    = "";
    $group->created     = common_sql_now();
    
    $result = $group->insert();
    
    print_r($result);
    
    $group->query('COMMIT');
    
    if ($la != "manchester") {
        $group = User_group::staticGet($result);
        $filename = Avatar::filename($result, ".gif", null, common_timestamp());
        symlink(INSTALLDIR."/Plings Logo Colours/".$img.".gif", INSTALLDIR."/avatar/".$filename);
        $group->setOriginal($filename);
    }
}

?>
