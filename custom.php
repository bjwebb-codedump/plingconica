<?php
/**
 * Plingconica - creating a Laconica instance full of plings data (http://plings.net)
 * Copyright (C) 2009 Ben Webb <bjwebb@freedomdreams.co.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once "functions.php";
require_once "config.php";
session_start();

function random_gen($length)
{
  $random= "";
  srand((double)microtime()*1000000);
  $char_list = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  $char_list .= "abcdefghijklmnopqrstuvwxyz";
  $char_list .= "1234567890";
  // Add the special characters to $char_list if needed

  for($i = 0; $i < $length; $i++)
  {
    $random .= substr($strset,(rand()%(strlen($strset))), 1);
  }
  return $random;
}

if ($_REQUEST["postcode"]) {
    $feed = "http://feeds.plings.net/xml.activity.php/7/postcode/".str_replace(" ","+",$_REQUEST["postcode"])."/?APIKey=".$apikey;
}
elseif ($_REQUEST["town"]) {
    include_once('fala.php');

    $la = getLA($_REQUEST['town']);
    $feed = "http://feeds.plings.net/xml.activity.php/7/la/".trim($la[0]["dgcode"])."?APIKey=".$apikey;
}
elseif ($_REQUEST["keyword"]) {
    $feed = $_SESSION["feed"];
    $keyword = $_REQUEST["keyword"];
}
elseif ($_REQUEST["category"]) {
    $feed = $_SESSION["feed"];
    $category = $_REQUEST["category"];
}
else {
    if ($_REQUEST["do"] == "reset" && !$_REQUEST["name"])
        $_SESSION["feed"] = "";
    else
        $feed = $_SESSION["feed"];
}
if ($_REQUEST["name"]) {
    $name = $_REQUEST["name"];
    
    $con = mysql_connect($db_host,$db_name,$db_pass);
    if (!$con) {
      die('Could not connect: ' . mysql_error());
    }
    if (mysql_select_db($db_db, $con)); else die(mysql_error());
    
    define('INSTALLDIR', realpath(dirname(__FILE__) . '/..'));
    define('LACONICA', true);
    require_once "../lib/common.php";
    
    $user = User::register(array('nickname' =>preg_replace("/[^0-9a-z]/", "", strtolower($name)), 'password' => $password, 'fullname' => $name));
    if (mysql_query("INSERT INTO `custom` (`feed`, `laconicaid`, `category`, `keyword`) VALUES ('".$feed."', '".$user->id."', '".$_SESSION["category"]."', '".$_SESSION["keyword"]."')"));
    else die(mysql_error());
    echo "<a href=\"../".$user->nickname."\">View the created user</a> or <a href=\"custom.php?do=reset\">start over</a>.";
}
elseif ($feed) {
    $_SESSION["feed"] = $feed;
    
    if ($keyword || $category) {
        echo "<a href=\"custom.php\">Back</a><br/>";
        $xml = simplexml_load_file($feed);
        if ($category) {
            $activities = $xml->xpath("//activity[contains(translate(categories/category, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz'), '".strtolower($category)."')]");
            $_SESSION["category"] = mysql_real_escape_string($category);
            $_SESSION["keyword"] = "";
        }
        elseif ($keyword) {
            $activities = $xml->xpath("//activity[contains(translate(keywords/keyword, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz'), '".strtolower($keyword)."')]");
            $_SESSION["category"] = "";
            $_SESSION["keyword"] = mysql_real_escape_string($keyword);
        }
        if (!count($activities)) {
            echo "Sorry, no results found.";
        }
        else {
            echo "<form method=\"post\">Name: <input type=\"text\" name=\"name\"><input type=\"submit\" value=\"Create\"></form>";
            echo "<ul>";
            $i=0;
            foreach ($activities as $activity){
                if ($i<30) echo "<li>".make_notice($activity)."</li>";
                else break;
                $i++;
            }
            if ($i==30) echo "Total Results: ".count($activities);
            echo "</ul>";
        }
    }
    else {
        echo "<a href=\"custom.php?do=reset\">Back</a><br/>";
        $xml = simplexml_load_file($feed."&MaxResults=20");
        if ($xml->queryDetails->TotalResults == 0) {
            echo "Sorry, no results were returned.";
        }
        elseif ($xml->queryDetails->TotalResults > 2000) {
            echo "Sorry, that postcode is not recognised.";
        }
        else {
            echo "Total Results: ".$xml->queryDetails->TotalResults."<br/>";
            echo "Narrow down results:";
            echo "<form method=\"post\">Keyword: <input type=\"text\" name=\"keyword\" /><input type=\"submit\" value=\"Submit\"/></form>";
            echo "<form method=\"post\">Category: <input type=\"text\" name=\"category\" /><input type=\"submit\" value=\"Submit\"/></form>";
            echo "<ul>";
            foreach ($xml->activities->activity as $activity){
                echo "<li>".make_notice($activity)."</li>";
            }
            echo "</ul>";
        }
    }
}
else {
?>
<form method="post">
    Town: <input type="text" name="town" />
    <input type="submit" value="Submit">
</form>
<form method="post">
    Postcode: <input type="text" name="postcode" />
    <input type="submit" value="Submit">
</form>
<?php } ?>
