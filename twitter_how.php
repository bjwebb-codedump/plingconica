<?php
/**
 * Plingconica - creating a Laconica instance full of plings data (http://plings.net)
 * Copyright (C) 2009 Ben Webb <bjwebb@freedomdreams.co.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
?>
To recieve notifications from a venue on twitter, you must first follow the <a href="http://twitter.com/plingstest1">plingstest1</a> user. Within a minute, it should be following you back. Once this is done, you can then private message this account the venue you wish to follow. For this venue, you would send the following message: 
<?php
if (preg_match("/^[0-9]+$/", $_GET["id"])) {
    require_once "config.php";
    $con = mysql_connect($db_host,$db_name,$db_pass);
    if (!$con) {
      die('Could not connect: ' . mysql_error());
    }
    if (mysql_select_db($db_db, $con)); else die(mysql_error());
    
    if ($result = mysql_query("SELECT * FROM `venues` WHERE `laconicaid`=".$_GET["id"], $con));
    else die(mysql_error());
    if ($row = mysql_fetch_assoc($result)) {
        echo "<pre>Venue ID: ".$row["id"]."</pre>";
    }
    else {
        echo "<pre>Sorry, subscribing to custom feeds is currently disabled due to the high message volume involved.</pre>";
        #echo "<pre>Laconica ID: ".$_GET["id"]."</pre>";
    }
}
?>