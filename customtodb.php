<?php
/**
 * Plingconica - creating a Laconica instance full of plings data (http://plings.net)
 * Copyright (C) 2009 Ben Webb <bjwebb@freedomdreams.co.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once "config.php";
$con = mysql_connect($db_host,$db_name,$db_pass);
if (!$con) {
  die('Could not connect: ' . mysql_error());
}
if (mysql_select_db($db_db, $con)); else die(mysql_error());

if ($result = mysql_query("SELECT * FROM `custom` WHERE `uptodate`=0", $con));
else die(mysql_error());
while ($row = mysql_fetch_assoc($result)) {
    $xml = simplexml_load_file($row["feed"]);
    if ($row["category"]) {
        $activities = $xml->xpath("//activity[contains(translate(categories/category, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz'), '".strtolower($row["category"])."')]");
    }
    elseif ($row["keyword"]) {
        $activities = $xml->xpath("//activity[contains(translate(keywords/keyword, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz'), '".strtolower($row["keyword"])."')]");
    }
    else {
        $activities = $xml->activities->activity;
    }
    foreach($activities as $activity) {
        if ($result = mysql_query("SELECT * FROM `custom_activities` WHERE `laconicaid`='".$row["laconicaid"]."' AND `aid`='".$activity->attributes()->id."'", $con));
        else die(mysql_error());
        if (mysql_fetch_row($result));
        else {
            if (mysql_query("INSERT INTO `custom_activities` (`laconicaid`, `aid`) VALUES ('".$row["laconicaid"]."', '".$activity->attributes()->id."')"));
            else die(mysql_error());
        }
    }
    if (mysql_query("UPDATE `custom` SET `uptodate`=1 WHERE `laconicaid`='".$row["laconicaid"]."'"));
    else die(mysql_error());
}

?>