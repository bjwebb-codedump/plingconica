<?php
/**
 * Laconica, the distributed open-source microblogging tool
 *
 * Plugin for Plingconica (showing twitter link atm)
 *
 * PHP version 5
 *
 * LICENCE: This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @category  Plugin
 * @package   Laconica
 * @author    Ben Webb <bjwebb@freedomdreams.co.uk>
 * @copyright 2009 Ben Webb
 * @license   http://www.fsf.org/licensing/licenses/agpl-3.0.html GNU Affero General Public License version 3.0
 * @link      http://laconi.ca/
 */

if (!defined('LACONICA')) {
    exit(1);
}

define('PLINGCONICAPLUGIN_VERSION', '0.1');

/**
 * Plingconica
 *
 * @category Plugin
 * @package  Laconica?
 * @author   Ben Webb
 * @license  MIT
 * @link     http://laconi.ca/
 *
 * @see      Event
 */

class PlingconicaPlugin extends Plugin
{
    function __construct($code=null)
    {
        parent::__construct();
    }

    function onStartShowSections($action)
    {
        $name = $action->trimmed('action');
        #print_r($action);

        if ($name == 'showstream') {
            $action->elementStart('div', array('id' => 'plingconica', 'class' => 'section'));
            $action->element('a', array('href' => "plings/twitter_how.php?id=".$action->profile->id), "Get notifications on twitter");
            $action->elementEnd('div');
        }

        return true;
    }

    function userAgent()
    {
        return 'PlingconicaPlugin/'.PLINGCONICAPLUGIN_VERSION.
          ' Laconica/' . LACONICA_VERSION;
    }
}
