<?php 
/**
 * Plingconica - creating a Laconica instance full of plings data (http://plings.net)
 * Copyright (C) 2009 Ben Webb <bjwebb@freedomdreams.co.uk>
 * Copyright (C) 2009 Tim Davies <tim@practicalparticipation.co.uk> - http://www.timdavies.org.uk/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

include_once "config.php";

	class BenDatabase {
		var $con;
		function BenDatabase($db) {
			global $db_host; global $db_name; global $db_pass;
			$this->con = mysql_connect($db_host,$db_name,$db_pass);
			if (!$this->con) {
			  die('Could not connect: ' . mysql_error());
			}
			if (mysql_select_db($db, $this->con)); else die(mysql_error());
		}
		function queryExec($q) {
			if (mysql_query($q, $this->con));
			else die(mysql_error);
		}
		function query($q) {
			if ($result = mysql_query($q, $this->con));
			else die(mysql_error);
			if ($row = mysql_fetch_assoc($result)) {
				return true;
			}
			else {
				return false;
			}
		}
		function arrayQuery($q) {
			if ($result = mysql_query($q, $this->con));
			else die(mysql_error);
			$arr = array();
			while ($row = mysql_fetch_assoc($result)) {
				$arr[] = $row;
			}
			return $arr;
		}
	}


	/**
		* function cleanInput($input)
		*/ 
	function clean($input) {
		if (ini_get('magic_quotes_gpc')) {
			//     $input = stripslashes($input);
		} 
		$input = trim($input);

		if(!is_numeric($input)) {
			//        $input = mysql_real_escape_string($input);
		} 
		return $input;
	} 

	/**
		* generateLink($town,$district)
		*/
	function generateLink($town,$district) {
		return $_SERVER['PHP_SELF'] . "?town=$town&district=$district";
	}

	/**
		* function getDB() - initiate the database
		* 
		*/
	function initDB() { 
		if ($db = new BenDatabase('places')) {
			$q = @$db->query('SELECT * FROM places');
			if ($q === false) {
				$db->queryExec('CREATE TABLE places (id int, town char(255), district char(255), la char(255), postcode char(10), lacode char (10), PRIMARY KEY (id));');
				return $db;
			} else {
				return $db;
			}
		} else {
			die($err);
		}
	}

	/**
		* checkDB()
		* 
		* Checks local sqLite cached datbase
		* 
		* Currently implemented: town and town & county search;
*/

	$checkOptions[0] = "checkDB";
	function checkDB($town,$district,$lacode,$postcode) {
		global $log;

		$log[] = "Checking DB";  

		$db = initDB();
		if($district) {
			$results = $db->arrayQuery("SELECT * FROM places WHERE town = '" . $town . "'");
			foreach($results as $result) {
				if($result['district'] == $district) {
					return array($result);
				} 
				return NULL;
			}
		} else {
			$results = $db->arrayQuery("SELECT * FROM places WHERE town LIKE '" .ucfirst($town) . "'");
		}

		$log[] = $results;

		return $results;
	}

	$checkOptions[1] = "checkAuthorityWards";
	function checkAuthorityWards($town,$district,$lacode,$postcode) {
	    global $log;
	        $log[] = "Checking Authorities and Wards: Authority";
	        $db = initDB();
	        $results = $db->arrayQuery("SELECT * FROM authorities WHERE official_name LIKE '" . $town . "' OR common_name LIKE '" . $town . "'");
		if(count($results)) {
		     $log[] = "Found an authority";
		     return array(array('la' => $results[0]['common_name'], 'district' => $results[0]['county']));
		} else {
		   $log[] = "Not an authority - check for wards...";
	           $results = $db->arrayQuery("SELECT * FROM wards WHERE ward_name LIKE '" . $town . "'");
		   if(count($results)) {	
		      $log[] = "Found a ward";
		      $log[] = $results; 
	              return array(array('la' => $results[0]['la_name']));
	           }

		}
	   	return $results;
	}

	/**
		* checkWiki()
		* 
		* Scrapes Wikipedia
		* 
		* 
		*/

	$checkOptions[2] = "checkWikiScrape";
	function checkWikiScrape($town,$district,$lacode,$postcode,$search = false) {
		global $log;
		$candidateLA = array("borough","unitary authority","london borough","governing body","district","metropolitan borough","type");
		$candidateCounty = array("county","metropolitan county","shire county","admin. county","admin county","administrative county","ceremonial county", "ceremonial and nonmetropolitan county","admin. county","admin county","administrative county");
	    $extradata['postcode'] = array("postcode district","postcode");  
	    $extradata['region'] = array("region");
	    $extradata['osref'] = array("os grid reference");

	    if($_REQUEST['disam'] & !$search) { $town = str_replace(" ","_",$town . ", ". $district); }

		if($search) {
			$url = "http://en.wikipedia.org/wiki/Special:Search/".str_replace(" ","_",$town);
			$log[] = "Using Search <a href='$url' target='_blank'>$url</a>";
		} else {
			$url = "http://en.wikipedia.org/wiki/".str_replace(" ","_",$town);
		}


		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 

		$resource = curl_exec($ch);

		include_once('simplehtmldom/simple_html_dom.php'); 
		$html = str_get_html($resource); 

		if($html->find("div[class=noarticletext]") || strpos($html->find("title",0),"Search results")) {
			$log[] = "No such wikipedia article. Searching... <a href='$url' target='_blank'>$url</a>";
			if(!$search) {
				checkWikiScrape($town,$district,$lacode,$postcode,true);
			} elseif($html->find("ul[class=mw-search-results]")) {
				$log[] = "Search results page";
				foreach($html->find("ul[class=mw-search-results]") as $list) {
					foreach($list->find("li") as $item) {
						if(strstr(strtolower($item->plaintext),"town") || strstr(strtolower($item->plaintext),"borough") ||strstr(strtolower($item->plaintext),"village") ||strstr(strtolower($item->plaintext),"city") ) {
							$town = $item->find("a",0)->title;
							$log[] = "Decided to fetch page for " . $town;
							checkWikiScrape($town,$district,$lacode,$postcode);
							break;
					}
				} 
					$log[] = "Looks like there were no good search results";
		   	}
		   }			
		} else {
				$log[] = "Wiki page returned";
				if($html->find('table[class=infobox]')) {
					$log[] = "Found Infobox";

					foreach($html->find('table[class=infobox]') as $infobox) {
						if(strstr($infobox->plaintext,"England") || strstr($infobox->plaintext,"United Kingdom")) {
							$log[] = "England or UK specific";

							foreach($infobox->find("tr") as $row) {
								$field_name = trim(strtolower(str_replace(":","",str_replace("-","",preg_replace("/&#?[a-z0-9]+;/i"," ",$row->childNodes(0)->plaintext))))); //We need to get rid of HTML entities (and tidy things up more on the next line)
								$infoarray[trim(strtolower(str_replace("-","",$field_name)))] = $row->childNodes(1)->plaintext;
							}
							$log[] = $infoarray;
							foreach($candidateLA as $checkThis) {
								if($infoarray[$checkThis]) { 
									$results['la'] = $infoarray[$checkThis];
									$log[] = "We've got an LA:" . $results['la'];
									break;
								}
							}
							if(!$county) { 
								foreach($candidateCounty as $checkThis) {
									if($infoarray[$checkThis]) { 
										$results['district'] = $infoarray[$checkThis];
										$log[] = "We've got a District too: " . $results['district'];
										break;
									}
								}
							}
							foreach($extradata as $key => $search) {
								foreach($search as $checkThis) {
									if($infoarray[$checkThis]) { 
										$results[$key] = $infoarray[$checkThis];
										$log[] = "We've got a $key too: " . $results[$key];
										break;
									}
								}
							}
							return array($results);
						} else {
							if(strstr(strtolower($resource),"disambiguation")) {
								$log[] = "Not UK specific - but may disambiguate... <a href='$url' target='_blank'>$url</a>";
								if($district) {
									$log[] = "Trying to district added...";
									return checkWikiScrape($town .", ".$district,$district,$lacode,$postcode,false);
								}
							}	
						}
					}
				} else {
					$log[] = "No infobox - <a href='$url' target='_blank'>$url</a>";
			
			
					  if(strstr($html->find("title",0),"Error")) {
				 		$log[] = "Wikipedia Error - try again later . $resource";
					    return null;
				      }

						if(strstr(strtolower($resource),"disambiguation")) {
							$log[] = "Not UK specific - but may disambiguate... <a href='$url' target='_blank'>$url</a>";
							//Try looking or 'dt' based lists...
						
							foreach($html->find("dt") as $dt) {
								if(strpos($dt->plaintext,"United Kingdom") || strpos($dt->plaintext,"England")) {
									$log[] = "Found something England related on disambiguation page...";
									foreach($dt->parent()->nextSibling()->find("li") as $li) {
									  $log[] = $li->plaintext;
									}
								}
							}
						
						} else {
							$log[] = "Not UK specific - abort <a href='$url' target='_blank'>$url</a>";
							return null;
						}
	          }


	    }
	   return $results;
	}


	//$checkOptions[3] = "checkGaze";
	function checkGaze($town,$district,$lacode,$postcode,$search = false) {
		global $log;
	
		$log[] = "Trying Gaze";

		$url = "http://gaze.mysociety.org/gaze-rest?f=find_places&country=GB&query=".$town;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 

		$resource = curl_exec($ch);
		//print_r(curl_getinfo($ch));

		$log[] = $resource;

	}


	$checkOptions[3] = "checkGeoNames";
	function checkGeoNames($town,$district,$lacode,$postcode,$search = false) {
		global $log;
		$log[] = "Trying GeoNames";

		$url = "http://ws.geonames.org/search?maxRows=5&country=GB&q=".$town;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 

		$resource = curl_exec($ch);
		//print_r(curl_getinfo($ch));

		$xml = simplexml_load_string($resource);
		foreach($xml->geoname as $location) {
			$url = "http://ws.geonames.org/hierarchy?geonameId=" . $location->geonameId;
			curl_setopt($ch, CURLOPT_URL,$url); 
			$location_detail = curl_exec($ch);
			$local_xml = simplexml_load_string($location_detail);
			$count = count($local_xml->geoname);
			$results[$n]['town'] = $local_xml->geoname[$count - 1]->name;
			$results[$n]['district'] = $local_xml->geoname[$count - 2]->name;		
			$n++;
		}
   
		$log[] = "Found $n possible location variations to choose from";
		return $results;

	}







	function getLA($town,$district = null,$lacode = null,$postcode=null) {
		//Get Query
		//CLEAN INPUT!!!
		$town = strtolower(clean($town));
		$district = strtolower(clean($district));
		$lacode = strtolower(clean($lacode));
		$postcode = strtolower(clean($postcode));
		global $log;
		global $checkOptions;

		foreach($checkOptions as $checkFunction) {
			if(function_exists($checkFunction)) {
				$result = call_user_func($checkFunction,$town,$district,$lacode,$postcode);
			} else {
				die($checkFunction . " does not exist");
			}
			$log[] = "Result Count: " . count($result);

			// We have a result!
			if(count($result) == 1) {
				$log[] = "Found unique record";
				$result = $result[0];
				if($result['la']) {
					$log[] = "Found LA: ". $result['la'];
					$la = trim($result['la']);
					  //Lets fill in the gaps in our information
					  $db = initDB();
					  $results = $db->arrayQuery("SELECT * FROM authorities WHERE official_name = '" . $la . "' OR common_name = '" . $la . "' LIMIT 1");
					  if($results) {
					  	$log[] = $results;
				    	return $results;
						break;
					  } else {
						$log[] = "But it looks like we were wrong. Not in our authoritative list!";
					  }
			
				} else {
					$log[] = "No LA - but checking if we can gather intelligence";
					if((!$town) && ($result['town'])) { $town = $result['town']; $log[] = "Updated town"; };
					if((!$district) && ($result['district'])) { $district = $result['district']; $log[] = "Updated district"; };
					if(!$lacode && $result['lacode']) { $lacode = $result['lacode']; $log[] = "Updated lacode"; };
					if(!$postcode && $result['postcode']) { $postcode = $result['postcode']; $log[] = "Updated postcode"; };
				}


				// Or we need to disambiguate?
			} elseif(count($result) > 1) { 
				$log[] = "Disambiguating";
				// Nothing found - let's try something else
			} else {
				$log[] = "No success with ".$checkFunction;     
			}

		}

	}
