<?php
/**
 * Plingconica - creating a Laconica instance full of plings data (http://plings.net)
 * Copyright (C) 2009 Ben Webb <bjwebb@freedomdreams.co.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once("functions.php");

if ($result = mysql_query("SELECT activities.*, custom_activities.laconicaid FROM activities JOIN custom_activities ON (activities.id=custom_activities.aid) LEFT OUTER JOIN doneids ON (activities.id=doneids.id AND doneids.source=custom_activities.laconicaid) WHERE doneids.id IS NULL AND starts>NOW() AND starts<'".$maxdate."' ORDER BY starts", $con));
else die(mysql_error());
while ($row = mysql_fetch_assoc($result)) {
    $laconicaid = $row["laconicaid"];
    if ($laconicaid) {
        post_notice($row["notice"], $laconicaid, "-custom");
        if (mysql_query("INSERT INTO `doneids` (`id`, `source`) VALUES ('".$row["id"]."', '$laconicaid');", $con));
        else die(mysql_error());
    }
}

?>
