<?php
/**
 * Plingconica - creating a Laconica instance full of plings data (http://plings.net)
 * Copyright (C) 2009 Ben Webb <bjwebb@freedomdreams.co.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once "config.php";
$con = mysql_connect($db_host,$db_name,$db_pass);
if (!$con) {
  die('Could not connect: ' . mysql_error());
}
if (mysql_select_db($db_db, $con)); else die(mysql_error());

define('INSTALLDIR', realpath(dirname(__FILE__) . '/..'));
define('LACONICA', true);
require_once "../lib/common.php";

function post_notice($text, $uid, $extra="") {
    if ($uid) {
        $notice = Notice::saveNew($uid, $text, "plingconica".$extra);
        if (is_object($notice)) {
            common_broadcast_notice($notice);
	    $notice->free();
	}
        unset($notice);
    }
    #print_r($notice);
}

function create_user($varray,$extra="") {
    global $con;
    $vid = $varray["id"];
    $vname = preg_replace("/[^A-Za-z0-9]/","",$varray["name"]).$extra;
    $vname = strtolower($vname);
    $user = User::register(array('nickname' => $vname, 'password' => $password, 'fullname' => $varray["name"], 'homepage' => $varray["plingsplaceslink"], 'location' => $varray["loc"]));
    
    if ($user) {
        $id = $user->id;
        $img = str_replace(" ","",ucwords($varray["laname"]));
        if (in_array($img, array("Birmingham", "Blackburn", "Blackpool", "Bolton", "Coventry", "Doncaster",
                "Durham", "Gateshead", "Halton", "Hull", "Islington", "Knowsley", "Leicester", "Lewisham", "Newcastle",
                    "Nottingham", "Salford", "SouthTyneside", "Stockport", "Wakefield", "Walsall"))) {
            $filename = Avatar::filename($id, ".gif", null, common_timestamp());
            symlink(INSTALLDIR."/Plings Logo Colours/".$img.".gif", INSTALLDIR."/avatar/".$filename);
            $profile = Profile::staticGet($id);
            $profile->setOriginal($filename);
        }
        return $id;
    }
    else {
        return false;
    }
}

function join_group($group, $userid) {
    $group = User_group::staticGet('nickname', $group);

    $member = new Group_member();
    
    $member->group_id   = $group->id;
    $member->profile_id = $userid;
    $member->created    = common_sql_now();
    
    $result = $member->insert();
    return $result;
}

function make_notice($activity) {
    $details = strip_tags($activity->Details);
    # if details in placeholder.....
    if ($activity->Name == "not known") $activity->venue->Name = $activity->venue->BuildingNameNo;
    $details .= " Venue: ".$activity->venue->Name;
    if ($activity->venue->BuildingNameNo) {
        $loc .= $activity->venue->BuildingNameNo;
        if (!is_numeric($activity->venue->BuildingNameNo))
            $loc .= ",";
    }
    if ($activity->venue->Street) $loc .= " ".$activity->venue->Street;
    if ($activity->venue->Town) $loc .= ", ".$activity->venue->Town;
    if ($activity->venue->PostTown && $activity->venue->Posttown != $activity->venue->Town) $loc .= ", ".$activity->venue->PostTown;
    if ($activity->venue->County) $loc .= ", ".$activity->venue->County;
    if ($activity->venue->Postcode) $loc .= ", ".$activity->venue->Postcode;
    $details .= ", ".$loc;
    $starts = strtotime($activity->Starts);
    $ends = strtotime($activity->Ends);
    $times = date("g:ia",$ends);
    if (date("a",$starts) == date("a",$ends)) $format = "g:i";
    else $format = "g:ia";
    $times = date($format,$starts)."-".$times;
    $day = date("D",$starts);
    
    $laname = strtolower($activity->venue->LAName);
    $town = $laname;
    
    $url = "http://m.plings.net/".$activity->attributes()->id;
    
    $tags = "";
    $i = 0;
    if ($activity->keywords->keyword) {
        foreach ($activity->keywords->keyword as $key) {
            $i ++;
            if ($i > 4) break;
            $key = preg_replace("/[^A-Za-z0-9]/","",$key);
            $tags = $tags."#".$key." "; 
        }
    }
    
    $head = $activity->Name." ($day $times): !".preg_replace("/[^A-Za-z0-9]/","",$town)." ";
    $tail = " ".$tags.$url;
    $space = 140 - strlen($head) - strlen($tail) - 2;
    $main = substr($details,0,$space)."..";
    
    return $head.$main.$tail;
}

$days = 2;

$maxdate = date("Y-m-d H:i:s",time()+$days*24*3600+3600);

?>
