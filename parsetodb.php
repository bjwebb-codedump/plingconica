<?php
/**
 * Plingconica - creating a Laconica instance full of plings data (http://plings.net)
 * Copyright (C) 2009 Ben Webb <bjwebb@freedomdreams.co.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once "config.php";
$con = mysql_connect($db_host,$db_name,$db_pass);
if (!$con) {
  die('Could not connect: ' . mysql_error());
}
if (mysql_select_db($db_db, $con)); else die(mysql_error());

define('INSTALLDIR', realpath(dirname(__FILE__) . '/..'));
define('LACONICA', true);
require_once "../lib/common.php";

function insert_into_db($tablename,$array) {
    global $con;
    
    if ($result = mysql_query("SELECT * FROM `".$tablename."` WHERE id='".$array["id"]."'", $con));
    else die(mysql_error());
    if (mysql_fetch_row($result)) {
        $sets = "";
        $i=0;
        foreach ($array as $field => $value) {
            if ($i != 0) {
                $sets .= ", ";
            }
            if (is_array($value)) $value = implode(";", $value);
            $sets .= "`".mysql_real_escape_string($field)."`='".mysql_real_escape_string($value)."'";
            $i++;
        }
        $query = "UPDATE `".$tablename."` SET ".$sets." WHERE id='".$array["id"]."'";
    }
    else {
        $fields = "";
        $values = "";
        $i=0;
        foreach ($array as $field => $value) {
            if ($i != 0) {
                $fields .= ", ";
                $values .= ", ";
            }
            if (is_array($value)) $value = implode(";", $value);
            $fields .= "`".mysql_real_escape_string($field)."`";
            $values .= "'".mysql_real_escape_string($value)."'";
            $i++;
        }
        $query = "INSERT INTO `".$tablename."` (".$fields.")"." VALUES "." (".$values.");";
    }
    
    if ($query) {
        if (mysql_query($query));
        else {
            #echo $query;
            die(mysql_error());
        }
        #echo "<br/>";
    }
    
}

function todb($aarray, $varray, $parray) {
    global $days;
    global $lacode;
    global $con;
    global $town;
    global $isPostcode;
    global $laconicaid;
    global $postcode;
    
    $details = strip_tags($aarray["details"]);
    # if details in placeholder.....
    if ($varray["name"] == "not known") $varray["name"] = $varray["buildingnameno"];
    $details .= " Venue: ".$varray["name"];
    if ($varray["buildingnameno"]) {
        $varray["loc"] .= $varray["buildingnameno"];
        if (!is_numeric($varray["buildingnameno"]))
            $varray["loc"] .= ",";
    }
    if ($varray["street"]) $varray["loc"] .= " ".$varray["street"];
    if ($varray["town"]) $varray["loc"] .= ", ".$varray["town"];
    if ($varray["posttown"] && $varray["posttown"] != $varray ["town"]) $varray["loc"] .= ", ".$varray["posttown"];
    if ($varray["county"]) $varray["loc"] .= ", ".$varray["county"];
    if ($varray["postcode"]) $varray["loc"] .= ", ".$varray["postcode"];
    $details .= ", ".$varray["loc"];
    $starts = strtotime($aarray["starts"]);
    $ends = strtotime($aarray["ends"]);
    $times = date("g:ia",$ends);
    if (date("a",$starts) == date("a",$ends)) $format = "g:i";
    else $format = "g:ia";
    $times = date($format,$starts)."-".$times;
    if ($times == "12:00-12:00am" || $times == "12:00am-12:00pm") $times = " all day";
    else $times = " ".$times;
    $day = date("D",$starts);
    
    $laname = strtolower($varray["laname"]);
    $town = $laname;
    
    $url = "http://m.plings.net/".$aarray["id"];
    
    $tags = "";
    $i = 0;
    if ($aarray["keyws"]) {
        foreach ($aarray["keyws"] as $key) {
            $i ++;
            if ($i > 4) break;
            $key = preg_replace("/[^A-Za-z0-9]/","",$key);
            $tags = $tags."#".$key." "; 
        }
    }
    
    $head = $aarray["name"]." ($day$times): !".preg_replace("/[^A-Za-z0-9]/","",$town)." ";
    $tail = " ".$tags.$url;
    $space = 140 - strlen($head) - strlen($tail) - 2;
    $main = substr($details,0,$space)."..";
    
    $aarray["notice"] = $head.$main.$tail;
    
    $aarray["vid"] = $varray["id"];
    $aarray["pid"] = $parray["id"];

    #print_r($aarray);
    #print_r($varray);
    #print_r($parray);

    insert_into_db("activities",$aarray);
    insert_into_db("venues", $varray);
    insert_into_db("providers", $parray);
}


function contents($parser, $data){
    global $aarray; global $varray; global $parray;
    global $act; global $ven; global $prov;
    global $tag;
    global $level;
    if (trim($data) != "") {
        if ($level == 4 && $act) $aarray[strtolower($tag)] = trim($data);
        if ($level == 5 && $ven) $varray[strtolower($tag)] = trim($data);
        if ($level == 5 && $prov) $parray[strtolower($tag)] = trim($data);
        if ($level == 5 && $tag == "KEYWORD") $aarray["keyws"][] = trim($data);
    }
}

function startTag($parser, $data, $attr){
    global $aarray; global $varray; global $parray;
    global $act; global $ven; global $prov;
    global $tag;
    global $level;
    $level++;
    $tag = $data;
    if ($data == "ACTIVITY") {
        $aarray = array();
        $aarray["id"] = $attr["ID"];
        $varray = array();
        $parray = array();
        $act = true;
    }
    if ($data == "VENUE") {
        $ven = true;
        $varray["id"] = $attr["ID"];
    }
    if ($data == "PROVIDER") {
        $prov = true;
        $parray["id"] = $attr["ID"];
    }
}

function endTag($parser, $data){
    global $aarray; global $varray; global $parray;
    global $act; global $ven; global $prov;
    global $tag;
    global $level;
    $level--;
    $tag = "";
    if ($data == "ACTIVITY") {
        todb($aarray, $varray, $parray);
        $act = false;
    }
    if ($data == "VENUE") {
        $ven = false;
    }
    if ($data == "PROVIDER") {
        $ven = false;
    }
}

function parse($feed) {
    $parser = xml_parser_create('');
    xml_set_element_handler($parser, "startTag", "endTag");
    xml_set_character_data_handler($parser, "contents"); 
    xml_parse($parser, $feed);
    xml_parser_free($parser);
}

$days = 7;
#echo "http://feeds.plings.net/xml.activity.php/$days?APIKey=$apikey";
$feed = file_get_contents("plingconica.xml");
parse($feed);

if (mysql_query("UPDATE `custom` SET `uptodate`=0 WHERE `uptodate`=1"));
else die(mysql_error());

?>
