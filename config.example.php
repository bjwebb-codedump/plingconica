<?php
/**
 * Plingconica - creating a Laconica instance full of plings data (http://plings.net)
 * Copyright (C) 2009 Ben Webb <bjwebb@freedomdreams.co.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

$apikey = ""; // Plings API key

$db_host = "localhost"; // DB Host
$db_name = ""; // DB Username
$db_pass = ""; // DB Password
$db_db = ""; // Database name

$twitterpass = ""; // Password of twitter account

?>

