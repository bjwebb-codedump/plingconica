<?php
/**
 * Plingconica - creating a Laconica instance full of plings data (http://plings.net)
 * Copyright (C) 2009 Ben Webb <bjwebb@freedomdreams.co.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

include "twitterlibphp/twitter.lib.php";

require_once "config.php";
$con = mysql_connect($db_host,$db_name,$db_pass);
if (!$con) {
  die('Could not connect: ' . mysql_error());
}
if (mysql_select_db($db_db, $con)); else die(mysql_error());

$twitter = new Twitter("plingstest1", $twitterpass);

$eds = explode("\n",$twitter->getFriendIDs());
$ers = explode("\n",$twitter->getFollowerIDs());
foreach (array_diff($ers,$eds) as $id) {
    $id = trim(strip_tags($id));
    print_r($twitter->createFriendship(array('id' => $id)));
    print_r($twitter->follow($id));
}

$messages = $twitter->getMessages();
$xml = simplexml_load_string($messages);
foreach ($xml->direct_message as $mess) {
    if (stripos($mess->text,"Venue ID: ") === 0) {
        preg_match("/[0-9]+/", (string)$mess->text , $matches);
        $venueid = $matches[0];
        if (mysql_query("INSERT INTO `twitter` (`username`, `venueid`) VALUES ('".$mess->sender_screen_name."', '".$venueid."');", $con));
        else die(mysql_error());
    }
    /*elseif (stripos($mess->text,"Laconica ID: ") === 0) {
        preg_match("/[0-9]+/", (string)$mess->text , $matches);
        $laconicaid = $matches[0];
        if (mysql_query("INSERT INTO `twitter` (`username`, `laconicaid`) VALUES ('".$mess->sender_screen_name."', '".$laconicaid."');", $con));
        else die(mysql_error());
    }*/
    $twitter->destroyMessage($mess->id);
}

$days = 2;
$maxdate = date("Y-m-d H:i:s",time()+$days*24*3600+3600);
if ($result = mysql_query("SELECT * FROM `twitter`", $con));
else die(mysql_error());
while ($row = mysql_fetch_assoc($result)) {
    if ($row["venueid"]) {
        if ($result2 = mysql_query("SELECT * FROM `activities` WHERE starts<'".$maxdate."' AND vid='".$row["venueid"]."' ORDER BY starts", $con));
        else die(mysql_error());
    }
    else {
        #if ($result2 = mysql_query("SELECT activities.* FROM `activities` JOIN `custom_activities` ON (custom_activities.aid=activities.id) WHERE starts<'".$maxdate."' AND custom_activities.laconicaid='".$row["laconicaid"]."' ORDER BY starts", $con));
        #else die(mysql_error());
        break;
    }
    while ($row2 = mysql_fetch_assoc($result2)) {
        if ($result3 = mysql_query("SELECT * FROM `doneids` WHERE `id`='".$row2["id"]."' AND `source`='twitter-".$row["username"]."'"));
        else die(mysql_error());
        if ($row3 = mysql_fetch_array($result3));
        else {
            $twitter->newMessage($row["username"], $row2["notice"]);
            if (mysql_query("INSERT INTO `doneids` (`id`, `source`) VALUES ('".$row2["id"]."', 'twitter-".$row["username"]."');", $con));
            else die(mysql_error());
        }
    }
}

?>