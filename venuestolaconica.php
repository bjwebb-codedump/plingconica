<?php
/**
 * Plingconica - creating a Laconica instance full of plings data (http://plings.net)
 * Copyright (C) 2009 Ben Webb <bjwebb@freedomdreams.co.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once("functions.php");

if ($result = mysql_query("SELECT activities.* FROM `activities` LEFT OUTER JOIN doneids ON (activities.id=doneids.id AND doneids.source='global') WHERE doneids.id IS NULL AND starts>NOW() AND starts<'".$maxdate."' ORDER BY starts", $con));
else die(mysql_error());
while ($row = mysql_fetch_assoc($result)) {
    if ($result2 = mysql_query("SELECT * FROM `venues` WHERE `id`=".$row["vid"], $con));
    else die(mysql_error());
    if ($row2 = mysql_fetch_assoc($result2)) {
        if ($row2["laconicaid"]) {
            $laconicaid = $row2["laconicaid"];
        }
        else {
            $i=1;
            while (true) {
                if ($i > 1) $extra = $i;
                else $extra = "";
                if ($laconicaid = create_user($row2, $extra)) {
                    if (mysql_query("UPDATE `venues` SET laconicaid=".$laconicaid." WHERE `id`=".$row2["id"], $con)) {
                        join_group(preg_replace("/[^A-Za-z0-9]/","",strtolower($row2["laname"])), $laconicaid);
                    }
                    else die(mysql_error());
                    break;
                }
                $i++;
            }
        }
        if ($laconicaid)
            post_notice($row["notice"], $laconicaid);
        #else
            #print_r($row2);
    }
    if (mysql_query("INSERT INTO `doneids` (`id`, `source`) VALUES ('".$row["id"]."', 'global');", $con));
    else die(mysql_error());
}

?>
